import React from "react";
import {BrowserRouter as Router} from "react-router-dom";
import BaseLayout from "./layout/BaseLayout";

function AppRouter() {
  return (
    <Router>
      <BaseLayout>
        <div>
          Hello my friend
        </div>
      </BaseLayout>
    </Router>
  );
}

export default AppRouter;
