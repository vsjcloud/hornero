import React from "react";
import {Navbar, Button, Classes} from "@blueprintjs/core";

const navbar: React.ReactElement = (
  <Navbar className={Classes.DARK}>
    <Navbar.Group>
      <Navbar.Heading>VSJ</Navbar.Heading>
      <Navbar.Divider/>
      <Button className={Classes.MINIMAL} icon="briefcase" text="Dự án"/>
    </Navbar.Group>
  </Navbar>
);

const BaseLayout: React.FC = (props) => {
  return (
    <React.Fragment>
      {navbar}
      {props.children}
    </React.Fragment>
  )
};

export default BaseLayout;
